package task07_JUNIT;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

public class FileCreatorAndWriter {
	
	boolean fileCreated;
	long filesize;
	
	public void createFileandWriteTo(String name, String toBeWritten) throws IOException {
		File file = new File(name);
		fileCreated=file.createNewFile();
		
		FileWriter writer = new FileWriter(file);
		writer.write(toBeWritten);
		writer.close();
		filesize=file.length();
	}

}
