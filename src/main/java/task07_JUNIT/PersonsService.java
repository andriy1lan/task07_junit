package task07_JUNIT;

public interface PersonsService {
	
	public String provideDirector();
	public int averageSalary();

}
