package task07_JUNIT;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class SalaryCalculator {
	
	public static final int CONS=17;
	
	PersonsService personservice;
	
	String director;
	String [] workers;
	int [] salaries;
	List<Integer> salarylist;
	
	public SalaryCalculator () {}
	public SalaryCalculator (String director, String [] workers, int [] salaries) {
		this.director = director;
		this.workers = workers;
		this.salaries = salaries;
	}
	
	public String getDirector() {
		return director;
	}
	
	public void setDirector (String director) {
		this.director = director;
	}
	
	
	public String[] getWorkers() {
		return workers;
	}
	
	public void setWorkers (String[] workers) {
		this.workers = workers;
	}
	
	public int[] getSalaries() {
		return salaries;
	}
	
	public void setSalaries (int[] salaries) {
		this.salaries = salaries;
	}
	
	public int getSalariesNumbers () {
		return salaries.length;
	}
	
	public int getWorkersIncludingDirectorNumbers () throws Exception  {
		if (this.director!=null)
		return workers.length + 1;
		else throw new Exception("No director");
	}
	
	public int findOveralSalaries() {
		int sum = 0;
		for(int i:salaries) {sum+=i;}
		return sum;
	}
		
	public int findAverageSalary1() {
		return personservice.averageSalary();
	}
	
	public int findAverageSalary() {
		int sum = 0;
		int n = salaries.length;
		for(int s:salaries) {sum+=s;}
		return sum / n;
	}
	
	public int findDirectorSalary() {
		return salaries[0];
	}
	
	public int findWorkersAverageSalary() {
		int sum = 0;
		int n = salaries.length-1;
		for(int i = 1; i < salaries.length; i++ ) {sum+=salaries[i];}
		return sum/n;
	}
	
	public boolean compareDirectorWithOthersSalaries() {
		return findDirectorSalary()>findWorkersAverageSalary();
	}
	
	
	public List<Integer> createListOfSalaries() {
		List<Integer> listi = new ArrayList<>();
		for(int i:salaries) {listi.add(i);}
		salarylist = listi;
		return listi;
	}
	
	
}
