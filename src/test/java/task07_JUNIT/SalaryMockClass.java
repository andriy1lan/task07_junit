package task07_JUNIT;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SalaryMockClass {
	
	@InjectMocks 
	SalaryCalculator scalculator = new SalaryCalculator();
	
	@Mock
	PersonsService personsService;
	
	@Test
	   public void testGetAverageSalary(){
	      when(personsService.averageSalary()).thenReturn(10000);
	      assertEquals(scalculator.findAverageSalary1(),10000);
	      verify(personsService).averageSalary();
	   }
	
}	