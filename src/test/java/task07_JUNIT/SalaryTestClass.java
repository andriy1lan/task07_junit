package task07_JUNIT;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import org.junit.Before;
import org.junit.Test;

public class SalaryTestClass {
	
	SalaryCalculator scalculator = new SalaryCalculator();
	
	@Before
		    public void init() {
		        scalculator.setDirector("I.Tymoschuk");
		        scalculator.setWorkers(new String[] {"R.Ivanchuk", "I.Seniv","O.Berezovska", "I.Leonenko","O.Koval"});
		        scalculator.setSalaries(new int[]{10500,7600,8500,9000,8100,9500});
	}
	
	
	        @Test
		    public void testArraysEquality() throws Exception {
		        assertFalse("Checking workers and salaries numbers", scalculator.getWorkers().length == scalculator.getSalaries().length);
		        assertTrue("Checking salaries and workers(with Director) numbers", scalculator.getSalariesNumbers() ==  scalculator.getWorkersIncludingDirectorNumbers());
	        }
	        
	        
	        @Test
	        public void testSalariesAndWorkersNumber() {
	        	assertEquals("Check the numbers of salaries", scalculator.getSalaries().length,6);
	        	assertEquals("Check the numbers of workers not including director", scalculator.getWorkers().length,5);
	        }
	        
	        @Test
	        public void testOveralSalaries() {
	        	assertEquals(scalculator.findOveralSalaries(),53200);
	        }
	        
	        @Test
	        public void testDirectorIsProvided() {
	        assertNotNull(scalculator.getDirector());   ;
	        }
	        
	        @Test
	        public void testWorker1isnotWorker2() {
	        	assertNotSame(scalculator.getWorkers()[1],scalculator.getWorkers()[0]);
	        }
	        
	        @Test(expected=IndexOutOfBoundsException.class)
	        public void testExceptionWhenRemovingNonExistent() {
	        	scalculator.createListOfSalaries().remove(7);
	        }
	        
	        @Test
	        public void testConstant() {
	        	assertEquals(scalculator.CONS,17);
	        }

}
