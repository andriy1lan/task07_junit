package task07_JUNIT;

import java.io.IOException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class VoidMethodsTest{
	
	FileCreatorAndWriter fc=new FileCreatorAndWriter();
	
	@Test
    public void testFileCreationAndWritting() throws IOException {
		int stringlenght = "Java is leading programming language".getBytes().length; //36 bytes
		fc.createFileandWriteTo("src/main/resources/Test.txt", "Java is leading programming language");
	    assertTrue("File 'Test.txt' was created", fc.fileCreated);
	    assertTrue("File 'Test.txt' was created", new java.io.File("src/main/resources/Test.txt").exists());
	    assertEquals("File was written",fc.filesize,36);
	}
	
}
	